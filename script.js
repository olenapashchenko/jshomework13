const img = document.querySelectorAll('.image-to-show')
const firstImage = img[0];
const lastImage = img[img.length - 1];
const allBtn = document.querySelector('.buttons')
const stop = document.querySelector('.stop')
const start = document.querySelector('.start')

const slider = () => {
    const currentImage = document.querySelector('.active');
    if (currentImage !== lastImage) {
        currentImage.classList.remove('active');
        currentImage.nextElementSibling.classList.add('active');
    } else {
        currentImage.classList.remove('active')
        firstImage.classList.add('active');
    }
}

let timer = setInterval(slider, 3000);

allBtn.addEventListener('click', (e) => {
    if (e.target === stop) {
        clearInterval(timer)
    } else if (e.target === start) {
        timer = setInterval(slider, 3000);
    }
})